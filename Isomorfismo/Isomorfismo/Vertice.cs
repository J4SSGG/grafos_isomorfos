﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isomorfismo
{
    class Vertice
    {
        private List<Edge> edges = new List<Edge>();

        public string Name { get; }

        public Vertice(string _name)
        {
            this.Name = _name;
        }

        public List<Edge> Edges()
        {
            this.Sort();
            return this.edges;
        }

        public void AddEdgeTo(int _destiny)
        {
            this.edges.Add(new Edge(_destiny));
            
        }

        private void Sort()
        {
            for (int i = 0; i < edges.Count; i++)
            {
                for (int j = i + 1; j < edges.Count; j++)
                {
                    if (edges[i].Destiny >= edges[j].Destiny)
                    {
                        Edge x = edges[i];
                        edges[i] = edges[j];
                        edges[j] = x;
                    }
                }
            }
        }
    }
}
