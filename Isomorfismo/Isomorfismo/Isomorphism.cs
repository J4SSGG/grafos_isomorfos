﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Isomorfismo
{
    class Isomorphism
    {
        public string AnalyzeGraphs(Graph _graph1, Graph _graph2)
        {
            try
            {
                if (!AnalyzeVertices(_graph1, _graph2)) // analyzes the number of vertices
                {
                    return "V"; //if graphs haven't the same number, returns a 'V' (from 'V'ertices).
                }
                if (!AnalyzeDegrees(_graph1, _graph2)) // analyzes the degree of all vertices of both graphs.
                {
                    return "D";
                }
                if (!AnalyzeEdges(_graph1, _graph2)) // analyzes the edges of both graphs.
                {
                    return "E";
                }
                if (AnalyzeVertices(_graph1, _graph2) && AnalyzeDegrees(_graph1, _graph2)) // I MAKE SURE THE LAST TWO IF'S ARE CORRECTLY EVALUATED.
                {
                    string result = this.AreIsomorphic(_graph1, _graph2); // This is the main method to evaluate if graphs are isomorphic.
                    if (result != null && result != "") // result should be something else than null or empty, otherwise, isomorphism function couldn't be found.
                    {
                        return result;
                    }
                    else
                    {
                        return "F";
                    }
                }
                return null;
            }
            catch
            {
                return "F";
            }
        }

        /// <summary>
        /// Recursive function that returns the permutations required from a set of values.
        /// </summary>
        /// <param name="_values"></param>
        /// One part of the values to permutate. (Ex: values from graph 1)
        /// <param name="_permutation"></param>
        /// Other part of the values to permutate. (Ex: values from graph2)
        /// <param name="_index"></param>}
        /// Value to start the permutation. When this is called, _index must be 0.
        /// <returns></returns>
        private List<string> Permutate(List<string> _values, List<string> _permutation, int _index)
        {
            Permutation Permutator = new Permutation(); // Object used to permutate. It is a new instance in every recursive call.
            if (_index + 1 >= _values.Count) // base case.
            {
                return Permutator.Permut(_values[_index]); // returns the last permutation generated.
            }
            else
            {
                List<string> aux = Permutator.Permut(_values[_index]); // auxiliary list to save the first part of permutations.
                for (int i = 0; i < aux.Count; i++) // I need to concatenate  the other parts of permutations. 
                {
                    List<string> aux2 = this.Permutate(_values, _permutation, _index + 1); // To get the other parts, we called this functions, chaging the index of the values required to permutate.
                    for (int j = 0; j < aux2.Count; j++)
                    {
                        _permutation.Add(aux[i] + "," + aux2[j]); // Concatenate every value from aux to the values generated the step before.
                    }
                }
            }
            return _permutation; // Returns permutations.
        }

        private void OrderOfPermutations(Graph _graph, ref List<int> _values)
        { 
            // Get the order (how many times the same degree appears. From lowest to highest) of the degrees of the graph required.
            // This is the order to take the vertices from graph, which later will be permutated.
            _values = _graph.ListOfDegrees();
        }

        private void TakeVertices(Graph _graph, List<int> _order, ref List<string> _values)
        {
            // Once the OrderOfPermutations is generated, this method selects the vertices that matches in that order.
            string vertices = "";
            for (int i = 0; i < _order.Count; i++) //Go over the values from 'Order'...
            {
                for (int j = 0; j < _graph.NumberOfVertices(); j++) // Rove all the vertices from graph.
                {
                    if (_graph.DegreeOfVertice(j) == _order[i] && _order[i] != 0) // Are the current value from order (the current degree) the same as the degree of the actual vertice?
                    {
                        vertices += j + ","; // Yes it is. So, added to the string.
                    }
                }
                if (vertices != "") // at least one vertice matches in the current order...
                {
                    _values.Add(vertices.Substring(0, vertices.Length - 1)); // added to the vertices list. this is the order list of vertices...
                    vertices = "";
                }
            }
        }

        private bool AnalyzeVertices(Graph _graph1, Graph _graph2)
        {
            return _graph1.NumberOfVertices() == _graph2.NumberOfVertices() ? true : false;
        }

        private bool AnalyzeDegrees(Graph _graph1, Graph _graph2)
        {
            // the number of different degrees couldn't be higher than the number of vertices. But in this case
            // i saved all the degrees. The arrays of both graphs must match, otherwise, degrees aren't the same.
            int[] x = new int[_graph1.NumberOfVertices()];
            int[] y = new int[_graph1.NumberOfVertices()];

            for (int i = 0; i < _graph1.NumberOfVertices(); i++) // Selecting the degrees. This method is called after vertices were analyzed.
            {
                x[i] = _graph1.DegreeOfVertice(i);
                y[i] = _graph2.DegreeOfVertice(i);
            }
            // the values, once sorted, must match.
            Array.Sort(x);
            Array.Sort(y);

            for (int i = 0; i < x.Length; i++) // rove both arrays.
            {
                if (x[i] != y[i])
                {
                    return false; // if some index doesn't match.
                }
            }
            return true;
        }

        private bool AnalyzeEdges(Graph _graph1, Graph _graph2)
        {
            if (_graph1.NumberOfEdges() == _graph2.NumberOfEdges())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string AreIsomorphic(Graph _graph1, Graph _graph2)
        {
            //some variables to get
            string result = null;

            // important variables to get
            List<int> OrderOfPermutations = new List<int>();
            List<string> g1_verticesInOrder = new List<string>();
            List<string> g2_verticesInOrder = new List<string>();
            List<string> Permutations = new List<string>();

            // getting the order to permutate...
            this.OrderOfPermutations(_graph1, ref OrderOfPermutations);

            // selecting those vertices that matches in the order...
            this.TakeVertices(_graph1, OrderOfPermutations, ref g1_verticesInOrder);
            this.TakeVertices(_graph2, OrderOfPermutations, ref g2_verticesInOrder);

            
            // basic cases that are much easier to evaluate....
            if (_graph1.IsKn() && _graph2.IsKn()) // All Kn graphs are isomorphic eachother.
            {
                // The order here doesn't matter. So, we only Join the values in any order. 
                if (_graph1.NumberOfVertices() == 1)
                {
                    return "0-0";
                }
                return string.Join(",", g1_verticesInOrder.ToArray()) + "-" + string.Join(",", g2_verticesInOrder.ToArray());
            }
            else if (OrderOfPermutations.Count == 1 && OrderOfPermutations[0] == 0)// All graphs with no edges are isomorphic eachother.
            {
                // The order here doesn't matter. So, we only Join the values in any order. But I still evaluate the solution, just to be sure.

                string p_solution = this.SameDegree_0(_graph1, _graph2);
                var _permutation = p_solution.Split('-');
                if (this.Analyze(_graph1, _graph2, _permutation[0], _permutation[1])) //verify the possible solution...
                {
                    return p_solution;
                }

            }
            // If graphs is 'iso-degree' (I mean, all vertices has the same degree) and this degree is equal to 1
            // I only need to get the ordered pairs, and join them eachother.
            else if (OrderOfPermutations.Count == 1 && OrderOfPermutations[0] == 1) 
            {
                string p_solution = this.SameDegree_1(_graph1, _graph2);
                var _permutation = p_solution.Split('-');
                if (this.Analyze(_graph1, _graph2, _permutation[0], _permutation[1])) //verify the possible solution...
                {
                    return p_solution;
                }
            }
            // If graphs are iso-degree, and the degree es even, well, better see the comments of SameDegree() method. 
            else if (g2_verticesInOrder.Count == 1 && g2_verticesInOrder.Count == 1 && OrderOfPermutations.Count == 1)
            {
                
                string p_solution = this.SameDegree(_graph1, _graph2, g1_verticesInOrder[0], g2_verticesInOrder[0]);
                var _permutation = p_solution.Split('-');
                if (this.Analyze(_graph1, _graph2, _permutation[0], _permutation[1])) //verify the possible solution...
                {
                    return p_solution;
                }
            }
            else
            {
                string p_solution = this.DegreeMethod(_graph1, _graph2, OrderOfPermutations, g1_verticesInOrder, g2_verticesInOrder);
                var _permutation = p_solution.Split('-');
                if (this.Analyze(_graph1, _graph2, _permutation[0], _permutation[1])) //verify the possible solution...
                {
                    return p_solution;
                }
            }

            // Make sure permutatation could be done

            for (int i = 0; i < g1_verticesInOrder.Count; i++)
            {
                if (g1_verticesInOrder[i].Length > 21 && g1_verticesInOrder[i] != "0")
                {
                    return "X";
                }
            }

             // IF ALL THE LAST CASES WEREN'T SUCCESFUL, THEN WE TRUST THE BASIC METHOD WORKS CORRECTLY..
            Permutations = this.Permutate(g2_verticesInOrder, Permutations, 0); // Generates only the possible solutions. Sometimes it doesn't generate the most effecient.
            result = this.MatrixMethod(_graph1, _graph2, g1_verticesInOrder, Permutations); // Analyze all the possible solutions..
            if (result != null)
            {

                return string.Join(",", g1_verticesInOrder.ToArray()) + "-" + result;
            }
            else
            {
                return "F";
            }
        }

        private string SameDegree_1(Graph _graph1, Graph _graph2)
        {
            string g1 = "";
            string g2 = "";
            for (int i = 0; i < _graph1.NumberOfVertices(); i++) // Recorre todos los vertices..
            {
                // necesito conocer si el valor del vertice ya esta en la cadena.
                var x = g1.Split(','); 
                var y = g2.Split(',');

                if (!x.Contains(i.ToString()) && !x.Contains(_graph1.Vertices()[i].Edges()[0].Destiny.ToString()))
                {
                    // Note que si no existe el vertice actual, entonces tampoco exisitirá el vertice al otro lado de la arista, pues estos son pares ordenados.
                    // Así que sino existe, se añada este par ordernado.
                    g1 += i + "," + _graph1.Vertices()[i].Edges()[0].Destiny + ",";
                }
                if (!y.Contains(i.ToString()) && !y.Contains(_graph2.Vertices()[i].Edges()[0].Destiny.ToString()))
                {
                    g2 += i + "," + _graph2.Vertices()[i].Edges()[0].Destiny + ",";
                }

            }

            return g1.Substring(0, g1.Length -1) + "-" + g2.Substring(0, g2.Length -1);
        }

        private string SameDegree_0(Graph _graph1, Graph _graph2)
        {
            // Este metodo es sencill. Cualquier combinacion de vertices es una solucion.
            // Solo recooremos los vertices para generar una solucion.
            string g1 = "";
            for (int i = 0; i < _graph1.NumberOfVertices(); i++)
            {
                if (i + 1 == _graph1.NumberOfVertices())
                {
                    g1 += i.ToString();
                }
                else
                {
                    g1 += i.ToString() + ",";
                }
            }

            return g1 + "-" + g1;
        }

        private string SameDegree(Graph _graph1, Graph _graph2, string _g1_vertices, string _g2_vertices)
        {
            // Este metodo es un tanto complejo.
            // Primero, solo funciona correctamente si el grafo tiene todos sus vertices con el mismo grado
            // En toería debe funcionar bajo cualquier numero de vertices, siempre que el grado sea par, pero si no lo es, suele o no hallar una solución...

            List<string> g1 = new List<string>();
            List<string> g2 = new List<string>();
            var g1_vertices = _g1_vertices.Split(',');
            var g2_vertices = _g2_vertices.Split(',');

            g1.Add(g1_vertices[0].Split(',')[0].ToString()); // tomamos un nodo inicial, que será el punto de partida. Este nodo será el mismo para ambos, dado OrderOfPermutation() y VerticesInOrder(), que genera la forma en que se tomarán estos vertices.
            g2.Add(g2_vertices[0].Split(',')[0].ToString());

            for (int i = 0; i < g1.Count; i++) // recorre la lista, que es la solución, y la cual crece conforme el bucle avanza...
            {
                List<Edge> g1_edges = _graph1.Vertices()[int.Parse(g1[i].ToString())].Edges(); // se obtienen las aristas del vertice que esta en indice i de la solucion.

                for (int j = 0; j < g1_edges.Count; j++) // recorremos esas aristas para añadir sus vertices destino a la solucion, aqui la solucion ha crecido, y el primer bucle no terminará sino hasta que haya recorrida todos los valores de la solcion, la cual dejara de crecer en algún punto.
                {
                    if (!g1.Contains(g1_edges[j].Destiny.ToString())) // siempre que el vertice no exista en la solucion, se añade. La cadena crecera hasta que todos los vertices hayan sido añadidos, y por ende tambien todas las aristas hayan sido recorridas.
                    {
                        g1.Add(g1_edges[j].Destiny.ToString());
                    }
                }
            }

            // exactamente igual al primer for, pero para el segundo grafo...
            for (int i = 0; i < g2.Count; i++)
            {
                List<Edge> g2_edges = _graph2.Vertices()[int.Parse(g2[i].ToString())].Edges();

                for (int j = 0; j < g2_edges.Count; j++)
                {
                    if (!g2.Contains(g2_edges[j].Destiny.ToString()))
                    {
                        g2.Add(g2_edges[j].Destiny.ToString());
                    }
                }
            }

                /* metodo experimental que resulta eficiente en ciertos casos, pero que considero no aporta mayor fluidez.
                for (int i = 0; i < g1_vertices.Length; i++)
                {
                    List<Edge> g1_edges = _graph1.Vertices()[int.Parse(g1_vertices[i])].Edges();
                    List<Edge> g2_edges = _graph2.Vertices()[int.Parse(g2_vertices[i])].Edges();

                    var x = g1.Split(',');
                    if (!x.Contains(i.ToString()))
                    {
                        g1 += i + ",";
                    }

                    x = g2.Split(',');
                    if (!x.Contains(i.ToString()))
                    {
                        g2 += i + ",";
                    }

                    for (int j = 0; j < _graph1.Vertices()[int.Parse(g1_vertices[i])].Edges().Count; j++)
                    {
                        x = g1.Split(',');
                        if (!x.Contains(g1_edges[j].Destiny.ToString()))
                        {
                            g1 += g1_edges[j].Destiny.ToString() + ",";
                        }
                    }
                    for (int j = 0; j < _graph2.Vertices()[int.Parse(g2_vertices[i])].Edges().Count; j++)
                    {
                        x = g2.Split(',');
                        if (!x.Contains(g2_edges[j].Destiny.ToString()))
                        {
                            g2 += g2_edges[j].Destiny.ToString() + ",";
                        }
                    }
                }*/

                return string.Join(",", g1.ToArray()) + "-" + string.Join(",", g2.ToArray());
        }

        private string DegreeMethod(Graph _graph1, Graph _graph2, List<int> _orderOfPermutations, List<string> _g1_verticesInOrder, List<string> _g2_verticesInOrder)
        {
            // Este metodo es un tanto complejo.
            // Primero, solo funciona correctamente si el grafo tiene todos sus vertices con el mismo grado.
            // En toería debe funcionar bajo cualquier numero de vertices....
            _orderOfPermutations.Remove(0);
            List<string> g1 = new List<string>();
            List<string> g2 = new List<string>();

            for (int i = 0; i < _orderOfPermutations.Count; i++) // recorre la lista, que es la solución, y la cual crece conforme el bucle avanza...
            {
                var g1_vertices = _g1_verticesInOrder[i].Split(',');
                var g2_vertices = _g2_verticesInOrder[i].Split(',');
                
                for (int j = 0; j < g1_vertices.Length; j++)
                {
                    List<Edge> g1_edges = _graph1.Vertices()[int.Parse(g1_vertices[j].ToString())].Edges();
                    List<Edge> g2_edges = _graph2.Vertices()[int.Parse(g2_vertices[j].ToString())].Edges();

                    if (!g1.Contains(g1_vertices[j].ToString()) && _graph1.DegreeOfVertice(int.Parse(g1_vertices[j])) == _orderOfPermutations[i]) // siempre que el vertice no exista en la solucion, se añade. La cadena crecera hasta que todos los vertices hayan sido añadidos, y por ende tambien todas las aristas hayan sido recorridas.
                    {
                        g1.Add(g1_vertices[j].ToString());
                    }
                    if (!g2.Contains(g2_vertices[j].ToString()) && _graph2.DegreeOfVertice(int.Parse(g2_vertices[j])) == _orderOfPermutations[i]) // siempre que el vertice no exista en la solucion, se añade. La cadena crecera hasta que todos los vertices hayan sido añadidos, y por ende tambien todas las aristas hayan sido recorridas.
                    {
                        g2.Add(g2_vertices[j].ToString());
                    }

                    for (int k = 0; k < g1_edges.Count; k++) // recorremos esas aristas para añadir sus vertices destino a la solucion, aqui la solucion ha crecido, y el primer bucle no terminará sino hasta que haya recorrida todos los valores de la solcion, la cual dejara de crecer en algún punto.
                    {
                        if (!g1.Contains(g1_edges[k].Destiny.ToString()) && _graph1.DegreeOfVertice(g1_edges[k].Destiny) == _orderOfPermutations[i]) // siempre que el vertice no exista en la solucion, se añade. La cadena crecera hasta que todos los vertices hayan sido añadidos, y por ende tambien todas las aristas hayan sido recorridas.
                        {
                            g1.Add(g1_edges[k].Destiny.ToString());
                        }
                        if (!g2.Contains(g2_edges[k].Destiny.ToString()) && _graph2.DegreeOfVertice(g2_edges[k].Destiny) == _orderOfPermutations[i]) // siempre que el vertice no exista en la solucion, se añade. La cadena crecera hasta que todos los vertices hayan sido añadidos, y por ende tambien todas las aristas hayan sido recorridas.
                        {
                            g2.Add(g2_edges[k].Destiny.ToString());
                        }
                    }
                }
            }
            return string.Join(",", g1.ToArray()) + "-" + string.Join(",", g2.ToArray());
        }

        private string MatrixMethod(Graph _graph1, Graph _graph2, List<string> _g1_order, List<string> _permutations)
        {
            string g1_order = string.Join(",", _g1_order.ToArray()); // reconstruimos el orden generado al inicio de AreIsomorphic()
            for (int i = 0; i < _permutations.Count; i++) // Recorre todas las soluciones generadas que se desean evaluar.
            {
                if (this.Analyze( _graph1, _graph2, g1_order, _permutations[i])) // analiza. 
                {
                    return _permutations[i];
                }
            }
            return null;
        }

        /// <summary>
        /// Este metodo analiza una solución contra la matriz de uno de los grafos.
        /// </summary>
        /// <param name="_graph1"></param>
        /// <param name="_graph2"></param>
        /// <param name="_g1_order"></param>
        /// <param name="_permutations"></param>
        /// <returns></returns>
        private bool Analyze(Graph _graph1, Graph _graph2, string _g1_order, string _permutations)
        {
            int[,] g1_matrix = _graph1.GetMatrix(); // obtenemos la matriz del grafo 1.
            List<Edge> g2_edgesOfVertice;
            List<string> g1_order = _g1_order.Split(',').ToList<string>(); // obtenemos la forma en que los vertices se intercambian para g1. Por ejemplo, el orden del grafo 1 es 1,2,3. Pues si la solucion para el grafo 2 es 5,4,6, a través de este metodo el 5 cambiara a 1, el 4 a 2. etc.
            List<string> g2_order = _permutations.Split(',').ToList<string>(); // orden del grafo 2.

            for (int i = 0; i < g1_order.Count; i++)
            {
                //CURRENT VERTICE
                int g2_vertice = int.Parse(g2_order[i]); // obtiene el vertice..
                int To_g1_vertice = int.Parse(g1_order[g2_order.IndexOf(g2_vertice.ToString())]); //lo cambia a partir del orden generado.
                //EDGES OF THE CURRENT VERTICE
                g2_edgesOfVertice = _graph2.Vertices()[g2_vertice].Edges(); // ahora si se selecciona las aristas del vertice correcto.

                for (int j = 0; j < g2_edgesOfVertice.Count; j++) // recorre esas aristas.
                {
                    int g2_destiny = g2_edgesOfVertice[j].Destiny; // se selecciona el vertice destino.
                    int To_g1_destiny = int.Parse(g1_order[g2_order.IndexOf(g2_destiny.ToString())]); // se cambia a partir del orden generado.

                    if (g1_matrix[To_g1_vertice, To_g1_destiny] != 1 && g1_matrix[To_g1_destiny, To_g1_vertice] != 1) // si no existe un 1 en la matriz del grafo 1, en la posicion del vertices de inicio y del vertice de destino del grafo dos, entonces esta no es una solucion. 
                    {
                        return false;
                    }
                }
            }

            return true; // llegara aqui cuando la solucion encaje.
        }

        public string AnalyzeUserSolution(Graph _graph1, Graph _graph2, string g1_order, string _permutations)
        {
            try
            {
                if (this.Analyze(_graph1, _graph2, g1_order, _permutations))
                {
                    return "Y";
                }
                else
                {
                    return "N";
                }
            }
            catch
            {
                return "X";
            }
        }
    }
}

