﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Isomorfismo
{
    class Drawing
    {
        private float[,] coordinates;
        private int CENTER_X = 0;
        private int CENTER_Y = 0; 
        private double RADIO = 0;
        private double MAX_DEGREE = 2 * Math.PI;

        public Bitmap GetImageOf(Graph _graph, PictureBox _image)
        {
            int x = _image.Width;
            int y = _image.Height;
            Bitmap imagen = new Bitmap(x, y);

            CENTER_X = x / 2;
            CENTER_Y = y / 2;
            RADIO = 0.6 * CENTER_X;


            this.GenerateCoordinates(_graph.NumberOfVertices());

            if (_graph.NumberOfVertices() > 50)
            {
                this.Failed(ref imagen);
            }
            else
            {
                this.DrawEdges(_graph, ref imagen);
                this.DrawVertices(_graph, ref imagen);
            }
            return imagen;
        }
        
        private void GenerateCoordinates(int _vertices)
        {
            coordinates = new float[_vertices, 2];

            for (int i = 0; i < _vertices; i++)
            {
                // x
                this.coordinates[i, 0] = Convert.ToSingle(CENTER_X + (Math.Cos((i * MAX_DEGREE / _vertices)) * RADIO));
                //y
                this.coordinates[i, 1] = Convert.ToSingle(CENTER_Y - (Math.Sin((i * MAX_DEGREE / _vertices)) * RADIO));
            }

        }

        private void DrawEdges(Graph _graph, ref Bitmap _image)
        {
            Pen Pen = new Pen(Color.DarkSlateGray,4);
            Graphics paint = Graphics.FromImage(_image);
            List<Vertice> vertices = _graph.Vertices();
            List<Edge> edges;
            int home;
            int destiny;

            for (int i = 0; i < _graph.NumberOfVertices(); i++)
            {
                edges = vertices[i].Edges();
                home = i;
                for (int j = 0; j < edges.Count; j++)
                {
                    destiny = edges[j].Destiny;
                    paint.DrawLine(Pen, coordinates[home, 0], coordinates[home, 1], coordinates[destiny, 0], coordinates[destiny, 1]);
                }
            }
        }

        private void DrawVertices(Graph _graph, ref Bitmap bitmap)
        {
            SolidBrush orageBrush = new SolidBrush(Color.OrangeRed);
            SolidBrush whiteBrush = new SolidBrush(Color.White);
            Graphics paint = Graphics.FromImage(bitmap);

            Font font = new Font("Arial", 11);

            for (int i = 0; i < _graph.NumberOfVertices(); i++)
            {
                Rectangle square = new Rectangle(Convert.ToInt32(coordinates[i, 0]) -15, Convert.ToInt32(coordinates[i, 1]) - 15, 30, 30);
                paint.FillEllipse(orageBrush, square);
                paint.DrawString(i.ToString(), font, whiteBrush, coordinates[i, 0] - 10, coordinates[i, 1] - 10);
            }
        }

        private void Failed(ref Bitmap bitmap)
        {
            Graphics paint = Graphics.FromImage(bitmap);
            Font font = new Font("Arial", 16);
            paint.DrawString("GRAPH IS TO BIG", font, new SolidBrush(Color.Black), CENTER_X, CENTER_Y);

        }

    }
}
