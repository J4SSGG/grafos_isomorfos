﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isomorfismo
{
    class Graph
    {
        private List<Vertice> vertices = new List<Vertice>(); // A graph is a group of vertices
        
        public Graph(int _count) // graph only exists if has at least ONE vertice. So every instance of a graph should contain the number of vertices.
        {
            for (int i = 0; i < _count; i++)
            {
                this.vertices.Add(new Vertice(i.ToString()));
            }
        }

        internal Vertice Vertice
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        internal Edge Edge
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public int NumberOfVertices()
        {
            return this.vertices.Count; // returns the number of vertices of this graph.
        }

        public int NumberOfEdges()  // returns the number of edges of this graph.
        {
            int count = 0;
            for (int i = 0; i < this.NumberOfVertices(); i++)
            {
                count += this.DegreeOfVertice(i);
            }
            return count / 2;
        }

        public int DegreeOfVertice(int _index) // returns the deegre of a specific vertice of this graph.
        {
            return this.vertices.Count > _index ? this.vertices[_index].Edges().Count : -1;
        }

        public List<int> ListOfDegrees()
        {
            //returns a list with the degrees in order of how many times the same degree repeats itself. Lowest first, highest last.
            List<string> degrees = new List<string>();
            List<int> list = new List<int>();

            for (int i = 0; i < this.NumberOfVertices(); i++)
            {
                if (!degrees.Contains(this.DegreeOfVertice(i).ToString())) // I only save the different degrees that exists in the graph, no how many times they appear.
                {
                    degrees.Add(this.DegreeOfVertice(i).ToString()); // I save the degree
                }
            }

            for (int i = 0; i < degrees.Count; i++) // this 'for' counts how many times the deegre value repeats. 
            {
                string item = degrees[i];
                int x = 0;
                for (int j = 0; j < this.NumberOfVertices(); j++)
                {
                    if (this.DegreeOfVertice(j).ToString() == item)
                    {
                        x++;
                    }
                }
                degrees[i] = degrees[i] + "," + x; // every value is concatenate with its counter.
            }
            //bubblesort
            // sort values and their counters.
            for (int i = 0; i < degrees.Count; i++)
            {
                
                for (int j = i + 1; j < degrees.Count; j++)
                {
                    var count = degrees[i].Split(',');
                    var count2 = degrees[j].Split(',');
                    if (int.Parse(count[1]) >= int.Parse(count2[1]))
                    {
                        string aux = degrees[j];
                        degrees[j] = degrees[i];
                        degrees[i] = aux;
                    }
                }
            }

            for (int i = 0; i < degrees.Count; i++)
            {
                string aux = degrees[i];
                list.Add(int.Parse(aux.Split(',')[0])); // clean the values
            }

            return list;
        }

        public bool NewEdge(string _edge) // sintax = "int(node:right),int(node:left)" without quotes. Example: 1,5
        {

            //This adds a new edge to graph.

            var _values = _edge.Split(',');
            int right_vertice = int.Parse(_values[0]);
            int left_vertice = int.Parse(_values[1]);
            if (right_vertice != left_vertice)
            {
                vertices[right_vertice].AddEdgeTo(left_vertice);
                vertices[left_vertice].AddEdgeTo(right_vertice);
            }
            else
            {
                return false;
            }
            return true;
        }

        public int[,] GetMatrix()
        {
            int[,] matrix = new int[vertices.Count, vertices.Count];

            for (int i = 0; i < vertices.Count; i++)
            {
                List<Edge> _subedges = this.vertices[i].Edges();

                for (int j = 0; j < _subedges.Count ; j++)
                {
                    matrix[i, _subedges[j].Destiny] = 1;
                }

            }
            return matrix;
        }

        public List<Vertice> Vertices()
        {
            return this.vertices; // returns the list of vertices of this graph.
        }

        public bool IsKn()
        {
            // this returns wheter a graph is or isn't a Kn.
            if (this.NumberOfVertices() > 0)
            {
                for (int i = 0; i < this.NumberOfVertices(); i++)
                {
                    if (this.DegreeOfVertice(i) + 1 != this.NumberOfVertices())
                    {
                        return false;
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public int EulerianPath()
        {
            // returns whether a graphs has an eulerian path (cycle);
            for (int i = 0; i < this.NumberOfVertices(); i++)
            {
                if (DegreeOfVertice(i) % 2 != 0 || DegreeOfVertice(i) == 0)
                {
                    return i;
                }               
            }
            return -1; // it means, yes, it has eulerian path.
        }

        public bool GenerateGraph(List<string> _values, ref Graph _graph)
        {
            // Get the info provided by the txt file and creates the graph.
            try
            {
                _graph = new Graph(int.Parse(_values[0])); // new instance of Graph class.
                for (int i = 1; i < _values.Count; i++)
                {
                    _graph.NewEdge(_values[i]);
                }
                return true;
            }
            catch 
            {
                return false;
            }
        }

    }
}
