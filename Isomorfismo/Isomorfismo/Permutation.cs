﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isomorfismo
{
    class Permutation
    {

        List<string> permutations;

        public List<string> Permut(int n) // esta funcion retorna permutaciones de 0 a n-1. 0,1,..,n-1 ... n-1,n-2,...1,0.
        {
            this.permutations = new List<string>(); // la lista se instancia a cada llamada, para evitar que contenga valores de instancias anteriores.
            string[] elements = new string[n];

            for (int i = 0; i < n; i++) // se genera la cadena a permuta, desde 0 hasta n - 1.
            {
                elements[i] = i.ToString();
            }

            this.Permut(elements, "", n, n);
            return this.permutations;
        }

        public List<string> Permut(string n) // esta función retorna permutaciones de una cadena, que contiene los elementos separados por comas (',').
        {
            this.permutations = new List<string>();
            string[] values = n.Split(',');
            this.Permut(values, "", values.Length, values.Length);
            return this.permutations;
        }

        private void Permut(string[] _elements, String _cadena, int n, int r) // metodo recursivo que genera las permutaciones según la cadena.
        {
            if (n == 0) // caso base, se ha recorrido todos los elementos de la cadena
            {
                this.permutations.Add(_cadena.Substring(0, _cadena.Length -1)); // se añade a las permutaciones
            }
            else
            {
                for (int i = 0; i < r; i++) // para cada valor en la cadena
                {
                    if (!_cadena.Contains(_elements[i])) // si dicho valor no existe aun en la permutación actual, entonces
                        Permut(_elements, _cadena + _elements[i] + ",", n - 1, r); // puede continuar la permutación, hasta llegar a n == 0, entonces esta será una nueva permutación.
                }
            }
        }
    }
}
