﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Isomorfismo
{
    class ReadFile
    {
        public bool Load(ref List<string> _values)
        {
            OpenFileDialog window = new OpenFileDialog(); // windows to select a file
            window.Title = "Select a file";
            window.ShowDialog();
            string name = window.FileName; // path of file

            if (name != "")
            {
                if (this.Read(name, ref _values))
                {
                    return true;
                }
                {
                    return false;
                }
                
            }
            else
            {
                return false;
            }

        }
        

        private bool Read(string _name, ref List<string> _values)
        {
            StreamReader reader = new StreamReader(_name); // object to read files.
            string line = reader.ReadLine(); // varible to save the string read.

            try
            {
                while (line != null && line != "") // line can not be null, otherwise file is empty or file has no more lines.
                {
                    if (line.IndexOf(',') < 0) // vertices should be firt in order to know the structure of the graph. 
                    {
                        if (int.Parse(line) > 0) // although the Graph class objet could have 0 vertices, this is not logic for real graphs, so graphs must have > 0 vertices
                        {
                            _values.Add(line);
                            line = reader.ReadLine(); // read next line
                            while (line != null) // after a new graph has been created, the file should contain edges of the graph, so we read them while file still have edges for the last graph created.
                            {
                                if (line.IndexOf(',') <= 0 && line != "")
                                {
                                    MessageBox.Show("El archivo contiene caracteres inválidos o la línea no es correcta: (" + line +").", "Error", MessageBoxButtons.OK);
                                    _values = null;
                                    reader.Close();
                                    return false;
                                }
                                if (line.Contains(_values[0].ToString()))
                                {
                                    MessageBox.Show("Se intenta añadir una arista a un vertice que no existe: (" + line + ").", "Error", MessageBoxButtons.OK);
                                    _values = null;
                                    reader.Close();
                                    return false;
                                }
                                if (line.Split(',')[0] == line.Split(',')[1])  // create new edge for the graph.
                                {
                                    MessageBox.Show("El archivo parece indicar que el grafo tiene un lazo: (" + line + ").", "Error", MessageBoxButtons.OK);
                                    _values = null;
                                    reader.Close();
                                    return false;
                                }
                                if (_values.Contains(line))
                                {
                                    MessageBox.Show("El archivo duplica una arista: (" + line + "). Se eliminará del grafo.", "Advertencia", MessageBoxButtons.OK);
                                }
                                _values.Add(line);
                                line = reader.ReadLine(); // next line.
                            }
                            reader.Close();
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("El archivo parece indicar que el grafo no tiene vértices.", "Advertencia", MessageBoxButtons.OK);
                            reader.Close();
                            _values = null;
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("El archivo parece indicar que el grafo no tiene vértices.", "Advertencia", MessageBoxButtons.OK);
                        _values = null;
                        reader.Close();
                        return false;
                    }
                }
                MessageBox.Show("El archivo esta vacío.", "Error", MessageBoxButtons.OK);
                _values = null;
                reader.Close();
                return false;
            }
            catch
            {
                MessageBox.Show("El archivo contiene caracteres inválidos. Verifique que no existan espacios en blanco: \n\t- Entre líneas.\n\t- Al final de la línea.\n\t- Al final del documento.", "Error", MessageBoxButtons.OK);
                reader.Close();
                return false;
            }
        }
    }
}
