﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isomorfismo
{
    class Edge
    {
        public int Destiny { get; }

        public Edge(int _destiny)
        {
            this.Destiny = _destiny;
        }

    }
}
