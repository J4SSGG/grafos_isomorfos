﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Isomorfismo
{
    public partial class UX : Form
    {

        Graph graph1;
        Graph graph2;
        ReadFile reader = new ReadFile();
        Isomorphism objIsomorphism = new Isomorphism();
        Drawing objDrawing = new Drawing();
        Bitmap img_graph1 = null;
        Bitmap img_graph2 = null;
        string function = "";

        public UX()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
         
        }        

        private void graph1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<string> values = new List<string>();
            if (reader.Load(ref values))
            {
                graph1 = new Graph(0);
                if (values != null && graph1.GenerateGraph(values, ref this.graph1))
                {
                    graph1ToolStripMenuItem.Enabled = false;
                    img_graph1 = objDrawing.GetImageOf(graph1, pictureBox1);
                    pictureBox1.Image = img_graph1;
                    comboBox1.SelectedIndex = 0;
                    this.LoadInfo(1);
                }
                else
                {
                    graph1 = null;
                }
            }
            this.EnableFunctions();
        }

        private void graph2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<string> values = new List<string>();
            if (reader.Load(ref values))
            {
                graph2 = new Graph(0);
                if (values != null && graph2.GenerateGraph(values, ref this.graph2))
                {
                    graph2ToolStripMenuItem.Enabled = false;
                    img_graph2 = objDrawing.GetImageOf(graph2, pictureBox1);
                    pictureBox1.Image = img_graph2;
                    comboBox1.SelectedIndex = 1;
                    this.LoadInfo(2);
                }
                else
                {
                    graph2 = null;
                }
            }
            this.EnableFunctions();
        }

        private void analyzeGraphsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (function == "")
            {
                this.Analyze();
            }
            else
            {
                if (MessageBox.Show("There is already a solution. Do you want to analyze again?", "Analyze graphs", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.Analyze();
                }
            }
        }

        private void Analyze()
        {

            function = this.objIsomorphism.AnalyzeGraphs(graph1, graph2);

            switch (function)
            {
                case "V":
                    this.lbl_status.Text = "There isn't isomorphism.";
                    this.lbl_status.BackColor = Color.Red;
                    this.lbl_reason.Text = "Graphs haven't a same number of vertices.";
                    break;
                case "D":
                    this.lbl_status.Text = "There isn't isomorphism.";
                    this.lbl_status.BackColor = Color.Red;
                    this.lbl_reason.Text = "Some vertices haven't the same degree.";
                    break;
                case "E":
                    this.lbl_status.Text = "There isn't isomorphism.";
                    this.lbl_status.BackColor = Color.Red;
                    this.lbl_reason.Text = "Number of edges aren't equal on both graphs.";
                    break;
                case "F":
                    this.lbl_status.Text = "There isn't isomorphism.";
                    this.lbl_status.BackColor = Color.Red;
                    this.lbl_reason.Text = "Isomorphism function couldn't be found.";
                    break;
                case "X":
                    MessageBox.Show("El programa no ha hallado una solución eficiente e intenta generar permutaciones de 11 o mas elementos. Se ha interrumpido el análisis para evitar que el programa falle.", "Alerta", MessageBoxButtons.OK);
                    break;
                default:
                    this.lbl_status.Text = "There's isomorphism!";
                    this.lbl_status.BackColor = Color.GreenYellow;
                    this.lbl_reason.Text = "Function found.";
                    this.btn_see.Visible = true;
                    var graphs = function.Split('-');
                    string[] graph1 = graphs[0].Split(',');
                    string[] graph2 = graphs[1].Split(',');
                    function = "\n\tGraph 1 -> Graph 2\n\n";
                    for (int i = 0; i < graph1.Length; i++)
                    {
                        function += "\t" + graph1[i] + " -> " + graph2[i] + "\n";
                    }
                    break;

            }
        }

        private void LoadInfo(int x)
        {
            switch (x)
            {
                case 1:
                    this.g1_complete.Text = graph1.IsKn() ? "Yes, is K" + graph1.NumberOfVertices() + "." : "No.";
                    this.g1_edges.Text = graph1.NumberOfEdges().ToString();
                    this.g1_euler.Text = graph1.EulerianPath() == -1 ? "Yes." : "No, vertice " + graph1.EulerianPath() + " hasn't even degree.";
                    this.g1_vertices.Text = graph1.NumberOfVertices().ToString();
                    break;
                case 2:
                    this.g2_complete.Text = graph2.IsKn() ? "Yes, is K" + graph2.NumberOfVertices() + "." : "No.";
                    this.g2_edges.Text = graph2.NumberOfEdges().ToString();
                    this.g2_euler.Text = graph2.EulerianPath() == -1 ? "Yes." : "No, vertice " + graph2.EulerianPath() + " hasn't even degree.";
                    this.g2_vertices.Text = graph2.NumberOfVertices().ToString();
                    break;
                default:
                    break;
            }
        }

        private void EnableFunctions()
        {
            if (graph1 != null && graph2 != null)
            {
                this.analyzeGraphsToolStripMenuItem.Enabled = true;
                this.lbl_reason.Text = "Graphs not analyzed.";
                this.tryYourSolutionToolStripMenuItem.Enabled = true;

            }
        }

        private void CleanInfo()
        {
            this.g1_complete.Text = "#";
            this.g1_edges.Text = "#";
            this.g1_euler.Text = "#";
            this.g1_vertices.Text = "#";
            this.g2_complete.Text = "#";
            this.g2_edges.Text = "#";
            this.g2_euler.Text = "#";
            this.g2_vertices.Text = "#";
            this.lbl_reason.Text = "Graphs not loaded.";
            this.lbl_status.Text = "Not found yet.";
            this.lbl_reason.BackColor = Color.Empty;
            this.lbl_status.BackColor = Color.Empty;
            this.btn_see.Visible = false;
            this.function = "";
            this.txt_userSolution.Text = "";
            this.txt_userSolution.Visible = false;
            this.tryYourSolutionToolStripMenuItem.Enabled = false;
            this.btn_userSolution.Visible = false;
            this.btn_ayuda.Visible = false;
        }

        private void cleanGraphsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.graph1 = null;
            this.graph2 = null;
            this.analyzeGraphsToolStripMenuItem.Enabled = false;
            this.graph1ToolStripMenuItem.Enabled = true;
            this.graph2ToolStripMenuItem.Enabled = true;
            this.pictureBox1.Image = null;
            this.img_graph1 = null;
            this.img_graph2 = null;
            this.CleanInfo();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (graph2 != null)
            {
                img_graph2 = objDrawing.GetImageOf(graph2, pictureBox1);
                pictureBox1.Image = img_graph2;
                comboBox1.SelectedIndex = 0;
            }
            if (graph1 != null)
            {
                img_graph1 = objDrawing.GetImageOf(graph1, pictureBox1);
                pictureBox1.Image = img_graph1;
                comboBox1.SelectedIndex = 0;
            }
            comboBox1.SelectedIndex = 0;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.pictureBox1.Image = null;
            if (comboBox1.SelectedIndex == 0)
            {
                this.pictureBox1.Image = img_graph1;
            }
            if (comboBox1.SelectedIndex == 1)
            {
                this.pictureBox1.Image = img_graph2;
            }
        }
        
        private void btn_see_Click(object sender, EventArgs e)
        {
            MessageBox.Show("The function is:\n" + this.function);
        }

        private void tryYourSolutionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (graph1 != null && graph2 != null && this.txt_userSolution.Visible == false)
            {
                this.txt_userSolution.Text = "";
                this.txt_userSolution.Visible = true;
                this.btn_userSolution.Visible = true;
                this.btn_ayuda.Visible = true;
                this.txt_userSolution.Focus();
            }
        }

        private void btn_userSolution_Click(object sender, EventArgs e)
        {
            if (txt_userSolution.Text == "")
            {
                txt_userSolution.Focus();
            }
            else if (!txt_userSolution.Text.Contains('-'))
            {
                MessageBox.Show("Debe ingresar ambas soluciones, separadas por un guión medio: ' - '. ", "Formato incorrecto", MessageBoxButtons.OK);
                txt_userSolution.Focus();
            }
            else if (txt_userSolution.Text.Split('-')[0].Split(',').Length != txt_userSolution.Text.Split('-')[1].Split(',').Length)
            {
                MessageBox.Show("Ambas soluciones deben contener el mismo número de vértices.", "Formato incorrecto", MessageBoxButtons.OK);
                txt_userSolution.Focus();
            }
            else
            {
                switch (objIsomorphism.AnalyzeUserSolution(graph1, graph2, txt_userSolution.Text.Split('-')[0], txt_userSolution.Text.Split('-')[1]))
                {
                    case "Y":
                        MessageBox.Show("La solución propuesta es correcta.", "Solucion correcta", MessageBoxButtons.OK);
                        break;
                    case "N":
                        MessageBox.Show("La solución propuesta es incorrecta.", "Solucion incorrecta", MessageBoxButtons.OK);
                        break;
                    case "X":
                        MessageBox.Show("La entrada en la cadena no tiene el formato correcto y ha provocado un error.", "Error", MessageBoxButtons.OK);
                        txt_userSolution.Focus();
                        break;
                    default:
                        break;
                }
            }
        }

        private void btn_ayuda_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Dada la siguiente solución:" +
                            "\n\n\t G1 -> G2 \n\t 1 -> 3 \n\t 2 -> 2 \n\t 3 -> 1" +
                            "\n\nSu entrada será: \n\n\t 1,2,3-3,2,1 \n\t\t ó \n\t 3,2,1-1,2,3" +
                            "\n\nNote que se separán los valores del primer grafo de los del segundo con un guión medio.", "Formato de entrada", MessageBoxButtons.OK);
        }

        /*********************************************************************/

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ChangeTextGraph1()
        {

        }

        private void loadFIleToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        
    }
}
