﻿namespace Isomorfismo
{
    partial class UX
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.loadFIleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graph1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graph2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analyzeGraphsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cleanGraphsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tryYourSolutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.g2_complete = new System.Windows.Forms.Label();
            this.g2_edges = new System.Windows.Forms.Label();
            this.g2_vertices = new System.Windows.Forms.Label();
            this.g2_euler = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.g1_euler = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.g1_complete = new System.Windows.Forms.Label();
            this.g1_edges = new System.Windows.Forms.Label();
            this.g1_vertices = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_see = new System.Windows.Forms.Button();
            this.lbl_reason = new System.Windows.Forms.Label();
            this.lbl_status = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.txt_userSolution = new System.Windows.Forms.TextBox();
            this.btn_userSolution = new System.Windows.Forms.Button();
            this.btn_ayuda = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadFIleToolStripMenuItem,
            this.analyzeGraphsToolStripMenuItem,
            this.cleanGraphsToolStripMenuItem,
            this.tryYourSolutionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1102, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // loadFIleToolStripMenuItem
            // 
            this.loadFIleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.graph1ToolStripMenuItem,
            this.graph2ToolStripMenuItem});
            this.loadFIleToolStripMenuItem.Name = "loadFIleToolStripMenuItem";
            this.loadFIleToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.loadFIleToolStripMenuItem.Text = "Load FIle";
            this.loadFIleToolStripMenuItem.Click += new System.EventHandler(this.loadFIleToolStripMenuItem_Click);
            // 
            // graph1ToolStripMenuItem
            // 
            this.graph1ToolStripMenuItem.Name = "graph1ToolStripMenuItem";
            this.graph1ToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.graph1ToolStripMenuItem.Text = "Graph 1";
            this.graph1ToolStripMenuItem.Click += new System.EventHandler(this.graph1ToolStripMenuItem_Click);
            // 
            // graph2ToolStripMenuItem
            // 
            this.graph2ToolStripMenuItem.Name = "graph2ToolStripMenuItem";
            this.graph2ToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.graph2ToolStripMenuItem.Text = "Graph 2";
            this.graph2ToolStripMenuItem.Click += new System.EventHandler(this.graph2ToolStripMenuItem_Click);
            // 
            // analyzeGraphsToolStripMenuItem
            // 
            this.analyzeGraphsToolStripMenuItem.Enabled = false;
            this.analyzeGraphsToolStripMenuItem.Name = "analyzeGraphsToolStripMenuItem";
            this.analyzeGraphsToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.analyzeGraphsToolStripMenuItem.Text = "Analyze Graphs";
            this.analyzeGraphsToolStripMenuItem.Click += new System.EventHandler(this.analyzeGraphsToolStripMenuItem_Click);
            // 
            // cleanGraphsToolStripMenuItem
            // 
            this.cleanGraphsToolStripMenuItem.Name = "cleanGraphsToolStripMenuItem";
            this.cleanGraphsToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.cleanGraphsToolStripMenuItem.Text = "Clean Graphs";
            this.cleanGraphsToolStripMenuItem.Click += new System.EventHandler(this.cleanGraphsToolStripMenuItem_Click);
            // 
            // tryYourSolutionToolStripMenuItem
            // 
            this.tryYourSolutionToolStripMenuItem.Enabled = false;
            this.tryYourSolutionToolStripMenuItem.Name = "tryYourSolutionToolStripMenuItem";
            this.tryYourSolutionToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.tryYourSolutionToolStripMenuItem.Text = "Try your solution!";
            this.tryYourSolutionToolStripMenuItem.Click += new System.EventHandler(this.tryYourSolutionToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel2);
            this.splitContainer1.Panel1MinSize = 400;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pictureBox1);
            this.splitContainer1.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel2_Paint);
            this.splitContainer1.Size = new System.Drawing.Size(1102, 517);
            this.splitContainer1.SplitterDistance = 400;
            this.splitContainer1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.groupBox2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(400, 517);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.g2_complete);
            this.groupBox3.Controls.Add(this.g2_edges);
            this.groupBox3.Controls.Add(this.g2_vertices);
            this.groupBox3.Controls.Add(this.g2_euler);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox3.Location = new System.Drawing.Point(3, 348);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(394, 166);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Graph 2";
            // 
            // g2_complete
            // 
            this.g2_complete.AutoSize = true;
            this.g2_complete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.g2_complete.Location = new System.Drawing.Point(153, 91);
            this.g2_complete.Name = "g2_complete";
            this.g2_complete.Size = new System.Drawing.Size(15, 16);
            this.g2_complete.TabIndex = 11;
            this.g2_complete.Text = "#";
            // 
            // g2_edges
            // 
            this.g2_edges.AutoSize = true;
            this.g2_edges.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.g2_edges.Location = new System.Drawing.Point(127, 65);
            this.g2_edges.Name = "g2_edges";
            this.g2_edges.Size = new System.Drawing.Size(15, 16);
            this.g2_edges.TabIndex = 10;
            this.g2_edges.Text = "#";
            // 
            // g2_vertices
            // 
            this.g2_vertices.AutoSize = true;
            this.g2_vertices.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.g2_vertices.Location = new System.Drawing.Point(135, 36);
            this.g2_vertices.Name = "g2_vertices";
            this.g2_vertices.Size = new System.Drawing.Size(15, 16);
            this.g2_vertices.TabIndex = 9;
            this.g2_vertices.Text = "#";
            // 
            // g2_euler
            // 
            this.g2_euler.AutoSize = true;
            this.g2_euler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.g2_euler.Location = new System.Drawing.Point(107, 117);
            this.g2_euler.Name = "g2_euler";
            this.g2_euler.Size = new System.Drawing.Size(15, 16);
            this.g2_euler.TabIndex = 8;
            this.g2_euler.Text = "#";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "Eulerian cycle:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Is it a complete graph: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Number of edges:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 16);
            this.label6.TabIndex = 3;
            this.label6.Text = "Number of vertices:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.g1_euler);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.g1_complete);
            this.groupBox2.Controls.Add(this.g1_edges);
            this.groupBox2.Controls.Add(this.g1_vertices);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 178);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(394, 164);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Graph 1";
            // 
            // g1_euler
            // 
            this.g1_euler.AutoSize = true;
            this.g1_euler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.g1_euler.Location = new System.Drawing.Point(107, 120);
            this.g1_euler.Name = "g1_euler";
            this.g1_euler.Size = new System.Drawing.Size(15, 16);
            this.g1_euler.TabIndex = 7;
            this.g1_euler.Text = "#";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 120);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "Eulerian cycle:";
            // 
            // g1_complete
            // 
            this.g1_complete.AutoSize = true;
            this.g1_complete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.g1_complete.Location = new System.Drawing.Point(153, 90);
            this.g1_complete.Name = "g1_complete";
            this.g1_complete.Size = new System.Drawing.Size(15, 16);
            this.g1_complete.TabIndex = 5;
            this.g1_complete.Text = "#";
            // 
            // g1_edges
            // 
            this.g1_edges.AutoSize = true;
            this.g1_edges.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.g1_edges.Location = new System.Drawing.Point(127, 64);
            this.g1_edges.Name = "g1_edges";
            this.g1_edges.Size = new System.Drawing.Size(15, 16);
            this.g1_edges.TabIndex = 4;
            this.g1_edges.Text = "#";
            // 
            // g1_vertices
            // 
            this.g1_vertices.AutoSize = true;
            this.g1_vertices.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.g1_vertices.Location = new System.Drawing.Point(135, 35);
            this.g1_vertices.Name = "g1_vertices";
            this.g1_vertices.Size = new System.Drawing.Size(15, 16);
            this.g1_vertices.TabIndex = 3;
            this.g1_vertices.Text = "#";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Is it a complete graph: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Number of edges:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Number of vertices:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_see);
            this.groupBox1.Controls.Add(this.lbl_reason);
            this.groupBox1.Controls.Add(this.lbl_status);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(394, 169);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Isomorphism Function";
            // 
            // btn_see
            // 
            this.btn_see.Location = new System.Drawing.Point(93, 105);
            this.btn_see.Name = "btn_see";
            this.btn_see.Size = new System.Drawing.Size(75, 23);
            this.btn_see.TabIndex = 12;
            this.btn_see.Text = "See!";
            this.btn_see.UseVisualStyleBackColor = true;
            this.btn_see.Visible = false;
            this.btn_see.Click += new System.EventHandler(this.btn_see_Click);
            // 
            // lbl_reason
            // 
            this.lbl_reason.AutoSize = true;
            this.lbl_reason.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_reason.Location = new System.Drawing.Point(79, 52);
            this.lbl_reason.Name = "lbl_reason";
            this.lbl_reason.Size = new System.Drawing.Size(134, 18);
            this.lbl_reason.TabIndex = 11;
            this.lbl_reason.Text = "Graphs not loaded.";
            // 
            // lbl_status
            // 
            this.lbl_status.AutoSize = true;
            this.lbl_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_status.Location = new System.Drawing.Point(69, 27);
            this.lbl_status.Name = "lbl_status";
            this.lbl_status.Size = new System.Drawing.Size(100, 18);
            this.lbl_status.TabIndex = 10;
            this.lbl_status.Text = "Not found yet.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(9, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 18);
            this.label13.TabIndex = 9;
            this.label13.Text = "Reason:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(9, 27);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 18);
            this.label14.TabIndex = 8;
            this.label14.Text = "Status:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(698, 517);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.SizeChanged += new System.EventHandler(this.Form1_Resize);
            // 
            // comboBox1
            // 
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Graph 1 Image",
            "Graph 2 Image"});
            this.comboBox1.Location = new System.Drawing.Point(981, 24);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // txt_userSolution
            // 
            this.txt_userSolution.BackColor = System.Drawing.SystemColors.Window;
            this.txt_userSolution.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_userSolution.Location = new System.Drawing.Point(368, 2);
            this.txt_userSolution.Margin = new System.Windows.Forms.Padding(0);
            this.txt_userSolution.MinimumSize = new System.Drawing.Size(2, 20);
            this.txt_userSolution.Name = "txt_userSolution";
            this.txt_userSolution.Size = new System.Drawing.Size(255, 20);
            this.txt_userSolution.TabIndex = 3;
            this.txt_userSolution.Visible = false;
            // 
            // btn_userSolution
            // 
            this.btn_userSolution.Location = new System.Drawing.Point(626, 1);
            this.btn_userSolution.Name = "btn_userSolution";
            this.btn_userSolution.Size = new System.Drawing.Size(22, 21);
            this.btn_userSolution.TabIndex = 4;
            this.btn_userSolution.Text = ">";
            this.btn_userSolution.UseVisualStyleBackColor = true;
            this.btn_userSolution.Visible = false;
            this.btn_userSolution.Click += new System.EventHandler(this.btn_userSolution_Click);
            // 
            // btn_ayuda
            // 
            this.btn_ayuda.Location = new System.Drawing.Point(649, 1);
            this.btn_ayuda.Name = "btn_ayuda";
            this.btn_ayuda.Size = new System.Drawing.Size(22, 21);
            this.btn_ayuda.TabIndex = 5;
            this.btn_ayuda.Text = "?";
            this.btn_ayuda.UseVisualStyleBackColor = true;
            this.btn_ayuda.Visible = false;
            this.btn_ayuda.Click += new System.EventHandler(this.btn_ayuda_Click);
            // 
            // UX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1102, 541);
            this.Controls.Add(this.btn_ayuda);
            this.Controls.Add(this.btn_userSolution);
            this.Controls.Add(this.txt_userSolution);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "UX";
            this.Text = "Graphs";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem loadFIleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graph1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graph2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analyzeGraphsToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ToolStripMenuItem cleanGraphsToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label g2_complete;
        private System.Windows.Forms.Label g2_edges;
        private System.Windows.Forms.Label g2_vertices;
        private System.Windows.Forms.Label g2_euler;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label g1_euler;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label g1_complete;
        private System.Windows.Forms.Label g1_edges;
        private System.Windows.Forms.Label g1_vertices;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_reason;
        private System.Windows.Forms.Label lbl_status;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btn_see;
        private System.Windows.Forms.ToolStripMenuItem tryYourSolutionToolStripMenuItem;
        private System.Windows.Forms.TextBox txt_userSolution;
        private System.Windows.Forms.Button btn_userSolution;
        private System.Windows.Forms.Button btn_ayuda;
    }
}

